define(["underscore", "d3", "jquery", "board"], function (_, d3, $, GameBoard) {
	"use strict";

	function zeroFilled(rows, cols) {
		var coll = [],
			row = [],
			x,
			y;

		for (y = 0; y < rows; y += 1) {
			row = [];
			for (x = 0; x < cols; x += 1) {
				row.push(0);
			}
			coll.push(row);
		}
		return coll;
	}

	function copyArray(array) {
		var copy = array.slice(0), i;
		for (i = 0; i < copy.length; i += 1) {
			copy[i] = array[i].slice(0);
		}
		return copy;
	}

	/*
		new Match(a[, b])

		Match game constructor
		ex. new Match(10, 10)
		ex. new Match([[0, 1, 1], [2, 3, 1], [4, 1, 2]])
	*/
	function Match(a, b, game) {
		if (typeof a === "object") {
			this.board = copyArray(a);
		} else if (typeof a === "number" && typeof b === "number") {
			this.board = zeroFilled(a, b);
		} else {
			throw new Error();
		}

		this.columns = this.board[0].length;
		this.game = game;
		this.history = [];
	}

	Match.prototype.toObjects = function () {
		var objects = [];
		var x, y;
		var board = this.board;

		for (y = 0; y < board.length; y += 1) {
			for (x = 0; x < board[y].length; x += 1) {
				objects.push({x: x, y: y, t: board[y][x]});
			}
		}

		return objects;
	};

	/*
		Match.drop()

		Attempts to "drop" tiles when there are zeroes underneath
		ex. [
				[1, 1, 1],
				[1, 0, 1],
				[1, 1, 1]
			]
		=>
			[
				[1, 0, 1],
				[1, 1, 1],
				[1, 1, 1]
			]
	*/
	Match.prototype.drop = function () {
		var x, y, v, board = this.board;
		for (y = board.length - 1; y >= 0; y -= 1) {
			for (x = 0; x < board[y].length; x += 1) {
				if (board[y][x] === 0) {
					for (v = y - 1; v >= 0; v -= 1) {
						if (board[v][x] !== 0) {
							this.swap([x, y], [x, v]);
						}
					}
				}
			}
		}
	};

	/*
		Match.tiles

		A collection of numbers that represent their frequency and type
	*/
	Match.prototype.tiles = [1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5];

	/*
		Match.empty()

		Sets the board to be zero filled
	*/
	Match.prototype.empty = function () {
		var x, y;
		for (y = 0; y < this.board.length; y += 1) {
			for (x = 0; x < this.board[y].length; x += 1) {
				this.place(0, [x, y], [0, 0]);
			}
		}
	};

	/*
		Match.fill()

		Changes empty (0) tiles to a random number found in Match.tiles
	*/
	Match.prototype.fill = function () {
		var x, y;

		for (y = 0; y < this.board.length; y += 1) {
			for (x = 0; x < this.board[y].length; x += 1) {
				if (this.board[y][x] === 0) {
					this.place(this.tiles[_.random(this.tiles.length - 1)], [x, y], [x, -1]);
				}
			}
		}
	};

	/*
		Match.play(a, b)

		Play a round
	*/
	Match.prototype.play = function (a, b) {
		var matches = [];
		if (this.isValid(a, b)) {
			this.swap(a, b);
			matches = this.matches(3);

			while (matches.length > 0) {
				this.destroy(matches);
				this.history.push(new Match(this.board));
				this.drop();
				this.history.push(new Match(this.board));
				this.fill();
				this.history.push(new Match(this.board));
				matches = this.matches(3);
			}

			if (this.isStuck()) {
				this.reset();
			}

			return true;
		}

		return false;
	};

	/*
		Match.destroy(as)

		Converts matching points to 0 (empty)
		ex. Match.destroy([[0, 0], [1, 0], [2, 0]])
	*/
	Match.prototype.destroy = function (matches) {
		var i, j;
		for (i = 0; i < matches.length; i += 1) {
			for (j = 0; j < matches[i].length; j += 1) {
				this.place(0, [matches[i][j][0], matches[i][j][1]], [0, 0]);
			}
		}
	};

	/*
		Match.direction(a, b)

		Determines the direction of a given swap
		ex. Match.direction([0, 1], [0, 0]) => [0, -1] (Up)
	*/
	Match.prototype.direction = function (a, b) {
		return [b[0] - a[0], b[1] - a[1]];
	};

	/*
		Match.isValid(a, b)

		Determines whether or not a given move results in a match
	*/
	Match.prototype.isValid = function (a, b) {
		if ((a[0] === b[0] && a[1] === b[1] + 1) ||
			(a[0] === b[0] && a[1] === b[1] - 1) ||
			(a[1] === b[1] && a[0] === b[0] + 1) ||
			(a[1] === b[1] && a[0] === b[0] - 1)) {

			var testBoard = new Match(this.board),
				matches,
				match,
				i;

			testBoard.swap(a, b);
			matches = testBoard.matches(3);

			if (matches.length > 0) {
				match = matches[0];
				for (i = 0; i < match.length; i += 1) {
					if ((match[i][0] === a[0] && match[i][1] === a[1]) ||
							(match[i][0] === b[0] && match[i][1] === b[1])) {
						return true;
					}
				}
			}
		}

		return false;
	};

	/*
		Match.randomTile()

		Returns a random tile number
	*/
	Match.prototype.randomTile = function () {
		return this.tiles[_.random(this.tiles.length - 1)];
	};

	/*
		Match.reset()

		Empties the board and replaces it with a playable, nonmatching board.
	*/
	Match.prototype.reset = function () {
		var testMatch = new Match(this.board[0].length, this.board.length),
			testBoard = testMatch.board,
			x,
			y,
			t;

		do {
			for (y = 0; y < testBoard.length; y += 1) {
				for (x = 0; x < testBoard[y].length; x += 1) {
					t = testMatch.randomTile();

					if (y > 1 && x > 1) {
						while ((t === testBoard[y][x - 1] && t === testBoard[y][x - 2]) ||
								(t === testBoard[y - 1][x] && t === testBoard[y - 2][x])) {
							t = testMatch.randomTile();
						}
					} else if (x > 1) {
						while (t === testBoard[y][x - 1] && t === testBoard[y][x - 2]) {
							t = testMatch.randomTile();
						}
					} else if (y > 1) {
						while (t === testBoard[y - 1][x] && t === testBoard[y - 2][x]) {
							t = testMatch.randomTile();
						}
					}

					testMatch.place(t, [x, y], [x, -1]);
				}
			}
		} while (testMatch.isStuck());

		for (y = 0; y < testBoard.length; y += 1) {
			for (x = 0; x < testBoard[y].length; x += 1) {
				this.place(testBoard[y][x], [x, y], [x, -1]);
			}
		}
	};

	/*
		Match.matches([min = 3])

		Returns a collection of collections, each collection a group of 2
		element vectors representing the x, y position of the matched
		tile.

		min limits matches to a minimum number
		(ex. min = 3, [0, 0, 1], [0, 0] is not included)
	*/
	Match.prototype.matches = function (min) {
		var board = this.board,
			visited = zeroFilled(board.length, board[0].length),
			matches = [],
			both = [],
			horz = [],
			vert = [],
			h,
			v,
			x,
			y;

		min = min || 3;

		for (y = 0; y < board.length; y += 1) {
			for (x = 0; x < board[y].length; x += 1) {
				if (visited[y][x] === 0 || true) {
					horz = []; // Horizontal matches
					vert = []; // Vertical matches

					// Push starting element into matches
					horz.push([x, y]);
					vert.push([x, y]);

					// Traverse to the right for matches
					for (h = x + 1; h < board[y].length; h += 1) {
						if (board[y][h] === board[y][x]) {
							visited[y][h] = 1;
							horz.push([h, y]);
						} else {
							break;
						}
					}

					// Traverse down for matches
					for (v = y + 1; v < board.length; v += 1) {
						if (board[v][x] === board[y][x]) {
							visited[v][x] = 1;
							vert.push([x, v]);
						} else {
							break;
						}
					}

					if (horz.length < min) {
						horz = [];
					}

					if (vert.length < min) {
						vert = [];
					}

					both = horz.concat(vert);

					if (both.length > 0) {
						matches.push(both);
					}
				}
			}
		}

		return matches;
	};

	/*
		Match.isStuck()

		Returns true if there are no more moves, false if otherwise
	*/
	Match.prototype.isStuck = function () {
		var board = this.board,
			offsets = [[1, 0], [0, 1], [-1, 0], [0, -1]],
			testBoard = new Match(board),
			destination,
			i,
			matches,
			origin,
			x,
			y;

		for (y = 0; y < board.length; y += 1) {
			for (x = 0; x < board[y].length; x += 1) {
				origin = [x, y];
				for (i = 0; i < offsets.length; i += 1) {
					if (x + offsets[i][0] >= 0 &&
							y + offsets[i][1] >= 0 &&
							x + offsets[i][0] < board[y].length &&
							y + offsets[i][1] < board.length) {
						destination = [x + offsets[i][0], y + offsets[i][1]];
						testBoard.swap(origin, destination);
						matches = testBoard.matches(3);
						testBoard.swap(destination, origin);
						if (matches.length > 0) {
							return false;
						}
					}
				}
			}
		}

		return true;
	};

	/*
		Match.swap(a, b)

		Swaps the tile from a to b, and b to a
	*/
	Match.prototype.swap = function (a, b) {
		var tA = this.board[a[1]][a[0]],
			tB = this.board[b[1]][b[0]];
		this.place(tA, b, a);
		this.place(tB, a, b);
	};

	/*
		Match.place(t, a)

		Puts a tile at a point in the board
	*/
	Match.prototype.place = function (t, a, b) {
		this.board[a[1]][a[0]] = t;
	};

	Match.prototype.init = function (svgID) {
		var inst = this;
		var columns = this.columns;
		var tileWidth = 32;

		var $svg = $(svgID);
		var svgWidth = $svg.width();

		var svg = d3.select(svgID);
		var stage = svg.append("g");

		var x = d3.scale.linear()
			.domain([0, columns - 1])
			.range([0, columns * tileWidth + 16]);

		var y = y = d3.scale.linear()
			.domain([0, columns - 1])
			.range([0, columns * tileWidth + 16]);

		var actions = {
			1: "Move",
			2: "Attack",
			3: "Powerup",
			4: "Raid",
			5: "Refresh"
		};

		var images = {
			0: "clear.png",
			1: "boot.png",
			2: "axe.png",
			3: "lightning2.png",
			4: "ship.png",
			5: "shield.png"
		}

		var selected = false, gems, origin, destination, tile, result, vec, direction, action;

		function update(data) {
			gems = stage.selectAll("image")
				.data(data, function (d) {
					return d.x + " " + d.y + " " + d.t;
				});

			gems.enter().append("image")
				.attr("width", 32)
				.attr("height", 32)
				.attr("xlink:href", function (d, i) {
					return "img/" + images[d.t];
				})
				.on("click", function (d, i) {

					stage.selectAll("image")
						.classed("selected", function () { return false; });

					d3.select(this)
						.classed("selected", function () { return true; });

					if (selected !== false) {

						origin = [selected.x, selected.y];
						destination = [d.x, d.y];
						tile = inst.board[selected.y][selected.x];

						if (tile === 5) {
							inst.reset();
							inst.history.push(inst);
						} else {
							result = inst.play(origin, destination);
							if (result === true) {
								vec = inst.direction(origin, destination);
								direction = false;

								if (vec[0] === -1) {
									direction = GameBoard.d.LEFT;
								} else if (vec[0] === 1) {
									direction = GameBoard.d.RIGHT;
								} else if (vec[1] === -1) {
									direction = GameBoard.d.UP;
								} else if (vec[1] === 1) {
									direction = GameBoard.d.DOWN;
								}

								switch (tile) {
									case 1:
										action = GameBoard.a.MOVE;
										break;
									case 2:
										action = GameBoard.a.ATTACK;
										break;
									case 3:
										action = GameBoard.a.RAGE;
										break;
									case 4:
										action = GameBoard.a.WIPE;
										break;
								}

								inst.game.trigger(action, direction);
							}
						}

						selected = false;
						stage.selectAll("image")
							.classed("selected", function () { return false; });
					} else {
						selected = d;
					}
				});

			gems.exit().remove();

			gems.attr("y", function (d) { return y(d.y); })
				.attr("x", function (d) { return x(d.x); });
		}

		inst.reset();
		update(inst.toObjects());
		window.setInterval(function () {
			if (inst.history.length > 0) {
				update(inst.history[0].toObjects());
				inst.history.splice(0, 1);
			}
		}, 200);
	};

	return Match;
});
