define(["backbone", "underscore", "jquery"], function(Backbone, _, $) {
    "use strict";

    var sum = function(lst) {
        return _.reduce(lst, function(memo, v) {
            return memo + v;
        }, 0);
    };

		var coord2s = function(c) {
				return "" + c.x + "," + c.y;
		};

		var s2coord = function(s) {
				return _.map(s.split(","), function(v){ return parseInt(v, 10)});
		};

		var getRand = function(min, max) {
			  return Math.floor(Math.random() * (max - min + 1) + min);
	  };

		var unit = function(v) {
				if (v) {
						return (v > 0) ? 1 : -1;
				}
				return 0;
		};

		var unitFrom = function(from, to) {
				var dx = to.x - from.x;
				var dy = to.y - from.y;
				return {
					x: unit(dx),
					y: unit(dy)
				}
		};

		var move_v = function(c1, c2) {
			return {
				x: c1.x + c2.x,
				y: c1.y + c2.y
			}
		};

		var MapThing = Backbone.Model.extend({
				getSkeletons: function() {
						var ret = [];
						_.each(this.get("skeletons"), function(v, k) {
								var coords = s2coord(k);
								if (v === 1) {
										ret.push({
												x: coords[0],
												y: coords[1]
										});
								}
						}, this);
						return ret;
				},
				getTiles: function() {
						var ret = [];
						_.each(this.get("map"), function(v, k) {
								var coords = s2coord(k);
								ret.push({
										x: coords[0],
										y: coords[1],
										v: v
								});
						}, this);
						return ret;
				},
				__spawnMonsterRandom: function(c) {
						c++;
						if (c > 1000) {
								return null;
						}
						var randc = {
								x: getRand(0, this.get("width")-1),
								y: getRand(0, this.get("height")-1)
						};
						if (this.collidesWithAny(["trash", "player", "skeletons", "map"], randc)) {
								return this.__spawnMonsterRandom(c);
						} else {
								return randc;
						}
				},
				spawnMonster: function() {
						var m = this.__spawnMonsterRandom(0);
						if (m) {
								this.get("skeletons")[coord2s(m)] = 1;
						};
				},
				isWin: function() {
						return this.collidesWith("pit", this.get("trash"));
				},
				collidesWithAny: function(targets, c) {
						return _.some(targets, function(target) {
								return this.collidesWith(target, c);
						}, this);
				},
				collidesWith: function(target, c) {
						switch(target) {
								case "trash":
								case "player":
								case "pit":
										var t = this.get(target);
										return (t.x === c.x && t.y === c.y);
										break;
								case "skeletons":
										return this.get(target)[coord2s(c)] === 1;
										break;
								case "map":
										return this.get(target)[coord2s(c)] !== 0;
										break;
						}
				},
				moveMonsters: function() {
						var newSkeletons = {};
						var p = this.get("player");
						_.each(this.getSkeletons(), function(skeleton) {
								newSkeletons[coord2s(skeleton)] = 1;
						}, this);
						_.each(this.getSkeletons(), function(skeleton) {
								var d = unitFrom(skeleton, p);
								var legits = [];
								if (d.x !== 0) {
										legits.push(move_v(skeleton, {x: d.x, y: 0}));
								}
								if (d.y !== 0) {
										legits.push(move_v(skeleton, {x: 0, y: d.y}));
								}
								legits = _.reject(legits, function(c) {
										return this.collidesWithAny(["map", "trash", "pit"], c) || (newSkeletons[coord2s(c)] === 1);
								}, this);
								if (legits.length) {
										var m = _.sample(legits);
										newSkeletons[coord2s(skeleton)] = 0;
										newSkeletons[coord2s(m)] = 1;
								}
						}, this);
						this.set("skeletons", newSkeletons);
				},
				kill: function(target) {
						var t = coord2s(target);
						var newSkeletons = {};
						_.each(this.getSkeletons(), function(skeleton) {
								var s = coord2s(skeleton);
								if (s !== t) {
										newSkeletons[s] = 1;
								}
						}, this);
						this.set("skeletons", newSkeletons);
				},
				killAll: function() {
						this.set("skeletons", {});
				},
				tick: function() {
						this.moveMonsters();
						if (this.collidesWith("skeletons", this.get("player"))) {
								this.board.trigger("lose");
						}
						if (getRand(0, 10) > 5) {
						    this.spawnMonster();
						}
				}
		});

    var lvlScriptToData = function(s, board) {
        s = s.replace(/(^\s+)|(\s+$)/g, "").split("\n");
        var map = {};
        var player = {};
        var trash = {};
        var pit = {};
				var skeletons = {};
        var width = 0;
        var height = s.length;
				var tile_count = 0;
        _.each(s, function(row, i) {
            width = Math.max(width, row.length);
            _.each(row, function(c, j) {
								var coord = {x:j, y:i};
                var s = coord2s(coord);
                if (c === "X") {
                    map[s] = 1;
                } else if (c === " ") {
                    map[s] = 0;
										tile_count++;
                } else if (c === "@") {
                    player = _.clone(coord);
                    map[s] = 0;
										tile_count++;
                } else if (c === "O") {
                    trash = _.clone(coord);
                    map[s] = 0;
										tile_count++;
                } else if (c === "_") {
                    pit = _.clone(coord);
                    map[s] = 0;
										tile_count++;
                }
            });
        });
        var map = new MapThing({
            width: width,
            height: height,
            map: map,
            player: player,
            trash: trash,
						skeletons: skeletons,
            pit: pit,
						tile_count: tile_count
        });
				map.board = board;
				return map;
    };

    var GameBoard = Backbone.View.extend({
        tileSize: 40,
        events: {
						"do_lose": "on_do_lose",
            "do_queue": "on_do_queue",
            "do_move": "on_do_move",
            "do_attack": "on_do_attack",
            "do_wipe": "on_do_wipe",
            "do_rage": "on_do_rage",
            "do_win": "on_do_win"
        },
        initialize: function(opts) {
            this.game = opts.game;
						this.alerter = opts.alerter;
            this.listenTo(this.game.model, "newLevel", this.loadLevel);
            this.map = null;
            _.each(this.events, function(v, k) {
                this.on(k, this[v], this);
            }, this);
        },
        loadLevel: function() {
            var lvl = this.game.model.get("level");
            var $map = $("#map-" + lvl);
            var $spec = $("#spec-" + lvl);
            if ($map) {
                this.map = lvlScriptToData($map.text(), this);
            } else {
								this.alerter.doAlert("YOU WIN VIKINGS", 3);
								this.alerter.doAlert("SO SEXY", 3);
								this.alerter.doAlert("BOOYAH", 3);
                this.map = null;
            }
            if (!$spec) {
                spec = $.noop;
            }

            this.render();
        },
        makeTile: function(txt, classes, c) {
            var $tile = $("<div>").
                text(txt).
                attr("class", classes).
                addClass("tile").css({
                    top: c.y * this.tileSize,
                    left: c.x * this.tileSize
            });
            this.$el.append($tile);
        },
        render: function() {
            if (_.isNull(this.map)) {
                return;
            }
            this.$el.empty();

            // render tiles
            this.$el.css({
                width: this.map.get("width") * this.tileSize,
                height: this.map.get("height") * this.tileSize
            });
            _.each(this.map.getTiles(), function(tile) {
                if (tile.v === 1) {
                    this.makeTile("X", "wall", tile);
                } else {
                    this.makeTile(" ", "floor", tile);
                }
            }, this);

            _.each(this.map.getSkeletons(), function(skeleton) {
                this.makeTile("&", "skeleton", skeleton);
            }, this);

            this.makeTile("#", "pit", this.map.get("pit"));
            this.makeTile("O", "trash", this.map.get("trash"));
            this.makeTile("@", "player", this.map.get("player"));

            if (this.map.isWin()) {
                this.trigger(GameBoard.a.WIN);
            }
        },
				on_do_lose: function() {
						this.alerter.doAlert("You Lose", 3);
						this.game.die();
				},
        on_do_move: function(d) {
            var v = GameBoard.d2c[d];

            var new_player = move_v(this.map.get("player"), v);
            var new_trash = _.clone(this.map.get("trash"));
            var collide_check = _.clone(new_player);
						if (this.map.collidesWith("trash", collide_check)) {
								new_trash = move_v(this.map.get("trash"), v);
                collide_check = _.clone(new_trash);
						}

						if (this.map.collidesWithAny(["map", "skeletons"], collide_check)) {
								this.alerter.doAlert("Collision");
						} else {
								this.alerter.doAlert("Moved!");
								this.map.set("player", new_player);
								this.map.set("trash", new_trash);
						}

						this.map.tick();
            this.render();
        },
        on_do_attack: function(d) {
						var v = GameBoard.d2c[d];
						var spot = move_v(this.map.get("player"), v);
						if (this.map.collidesWith("skeletons", spot)) {
								this.alerter.doAlert("Killed!");
								this.map.kill(spot);
						} else {
								this.alerter.doAlert("*whiff*");
						}
						this.map.tick();
            this.render();
        },
				on_do_wipe: function(d) {
						this.alerter.doAlert("GENOCIDE!");
						this.alerter.doAlert("All things keeled!");
						this.map.killAll();
						this.map.tick();
            this.render();
				},
				on_do_rage: function(d) {
						this.alerter.doAlert("Raawrrrrrr");
						this.alerter.doAlert("Ragerrific");
						this.map.killAll();
						this.map.tick();
            this.render();
				},
        on_do_win: function() {
						this.alerter.doAlert("VIKINGS FTW");
						this.alerter.doAlert("Level Up!", 2);
            this.game.model.trigger("levelUp");
        }
    });

    GameBoard.d = {
        UP: 0,
        RIGHT: 1,
        DOWN: 2,
        LEFT: 3
    };

    GameBoard.d2c = {};
    GameBoard.d2c[GameBoard.d.UP] = {x:0, y:-1};
    GameBoard.d2c[GameBoard.d.RIGHT] = {x:1, y:0};
    GameBoard.d2c[GameBoard.d.DOWN] = {x:0, y:1};
    GameBoard.d2c[GameBoard.d.LEFT] = {x:-1, y:0};

    GameBoard.d2s = _.invert(GameBoard.d);

    GameBoard.a = {
        MOVE: "do_move",
        ATTACK: "do_attack",
        QUEUE: "do_queue",
        WIPE: "do_wipe",
        RAGE: "do_rage",
        WIN: "do_win"
    };

    GameBoard.a2s = _.invert(GameBoard.a);

    return GameBoard;
});
