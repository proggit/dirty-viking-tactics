require.config({
    baseUrl: "./src",
    paths: {
        "jquery": "lib/jquery-1.10.2.min",
        "underscore": "lib/underscore-min",
        "backbone": "lib/backbone-min",
        "d3": "lib/d3.v3.min"
    },
    shim: {
        "underscore": {exports: "_"},
        "backbone": {deps: ["underscore", "jquery"], exports: "Backbone"},
        "d3": {exports: "d3"}
    },
    waitSeconds: 15
});

require(["backbone", "board", "match"], function(Backbone, GameBoard, Match) {
    "use strict";

		var Alerter = Backbone.View.extend({
				initialize: function() {
						this.$el.css({
								position: "relative",
								zIndex: 99999
						});
				},
				doAlert: function(msg, d) {
						d = d || 1;
						var $e = $("<div>").text(msg);
						var x = Math.floor(Math.random() * 100);
						var y = Math.floor(Math.random() * 100);
						var f = Math.floor(Math.random() * 20) + 40;
						var r = Math.floor(Math.random() * 20) - 10;
						var r2 = Math.floor(Math.random() * 20) - 10;
						$e.css({
							top: y,
							left: x,
							fontSize: f,
							transform: "rotate(" + r + "deg)"
						});
						this.$el.append($e);
						window.setTimeout(function() {
						  $e.css({
								transform: "rotate(" + r2 + "deg)",
								fontSize: f + 20
							});
						}, 0);
						$e.fadeIn(500 * d).promise().done(function() {
							$e.fadeOut(function() {
								$e.remove();
							});
						});
				}
		});


    var DVTGameState = Backbone.Model.extend({
        defaults: {
            level: 0
        },
        initialize: function() {
            this.on("levelUp", this.levelUp, this);
        },
        levelUp: function() {
            this.set("level", this.get("level") + 1);
            this.trigger("newLevel");
        }
    });

    var DVTGame = Backbone.View.extend({
        initialize: function() {
						var alerter = new Alerter({
								el: $("#alerts")
						});

            this.gameBoard = new GameBoard({
                game: this,
                el: $("div#gameboard"),
								alerter: alerter
            });

            // proxy events to children
            this.on("all", function(evt) {
                this.gameBoard.trigger.apply(this.gameBoard, arguments);
            }, this);

            this.matchGame = new Match(6, 6, this);
            this.matchGame.init("#controls");
            this.model.trigger("levelUp");
        },
				die: function() {
						this.model.set("level", 0);
						this.model.trigger("levelUp");
				}
    });

    $().ready(function() {
        var game = new DVTGame({
            model: new DVTGameState()
        });
        window.game = game;
    });
});
