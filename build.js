({
    baseUrl: "./src",
    name: 'main',
    out: 'build/main-built.js',
    paths: {
        "jquery": "lib/jquery-1.10.2.min",
        "underscore": "lib/underscore-min",
        "backbone": "lib/backbone-min",
        "d3": "lib/d3.v3.min"
    },
    shim: {
        "underscore": {exports: "_"},
        "backbone": {deps: ["underscore", "jquery"], exports: "Backbone"},
        "d3": {exports: "d3"}
    }
})
